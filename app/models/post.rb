class Post < ApplicationRecord
  has_many :comentarios, dependent: :destroy
  validates :titulo, :presence => true
  validates :genero, :presence => true
  validates :resumo, :presence => true, :length => { :minimum => 10 }
  validates :conteudo, :presence => true, :length => { :minimum => 10 }

  scoped_search on: [:titulo, :genero]
end
